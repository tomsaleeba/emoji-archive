#!/bin/bash
set -euo pipefail
cd "$(dirname "$0")"

readmePath=README.md

echo "> A stash of my favourite emoji. I did't make any of these, only found them" > $readmePath

for curr in *.gif *.png; do
  echo "![]($curr)" >> $readmePath
done
